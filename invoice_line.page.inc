<?php

/**
 * @file
 * Contains invoice_line.page.inc.
 *
 * Page callback for Invoice line entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Invoice line templates.
 *
 * Default template: invoice_line.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_invoice_line(array &$variables) {
  // Fetch InvoiceLine Entity Object.
  $invoice_line = $variables['elements']['#invoice_line'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
