<?php

namespace Drupal\einvoice\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Invoice line entities.
 *
 * @ingroup einvoice
 */
interface InvoiceLineInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Invoice line name.
   *
   * @return string
   *   Name of the Invoice line.
   */
  public function getName();

  /**
   * Sets the Invoice line name.
   *
   * @param string $name
   *   The Invoice line name.
   *
   * @return \Drupal\einvoice\Entity\InvoiceLineInterface
   *   The called Invoice line entity.
   */
  public function setName($name);

  /**
   * Gets the Invoice line creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Invoice line.
   */
  public function getCreatedTime();

  /**
   * Sets the Invoice line creation timestamp.
   *
   * @param int $timestamp
   *   The Invoice line creation timestamp.
   *
   * @return \Drupal\einvoice\Entity\InvoiceLineInterface
   *   The called Invoice line entity.
   */
  public function setCreatedTime($timestamp);

}
