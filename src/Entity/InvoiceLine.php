<?php

namespace Drupal\einvoice\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserInterface;

/**
 * Defines the Invoice line entity.
 *
 * @ingroup einvoice
 *
 * @ContentEntityType(
 *   id = "invoice_line",
 *   label = @Translation("Invoice line"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\einvoice\InvoiceLineListBuilder",
 *     "views_data" = "Drupal\einvoice\Entity\InvoiceLineViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\einvoice\Form\InvoiceLineForm",
 *       "add" = "Drupal\einvoice\Form\InvoiceLineForm",
 *       "edit" = "Drupal\einvoice\Form\InvoiceLineForm",
 *       "delete" = "Drupal\einvoice\Form\InvoiceLineDeleteForm",
 *     },
 *     "access" = "Drupal\einvoice\InvoiceLineAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\einvoice\InvoiceLineHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "invoice_line",
 *   admin_permission = "administer invoice line entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/invoice_line/{invoice_line}",
 *     "add-form" = "/admin/structure/invoice_line/add",
 *     "edit-form" = "/admin/structure/invoice_line/{invoice_line}/edit",
 *     "delete-form" = "/admin/structure/invoice_line/{invoice_line}/delete",
 *     "collection" = "/admin/structure/invoice_line",
 *   },
 *   field_ui_base_route = "invoice_line.settings"
 * )
 */
class InvoiceLine extends ContentEntityBase implements InvoiceLineInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Invoice line entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['invoice_line_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Line id'))
      ->setDisplayOptions('form', [
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['line_textual_note'] = BaseFieldDefinition::create('string')
      ->setLabel(t("Textual note"))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['invoiced_quantity'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Quantity'))
      ->setDisplayOptions('form', [
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['quantity_unit_of_measure'] = BaseFieldDefinition::create('string')
      ->setLabel(t("Quantity unit of measure"))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)

    $fields['invoice_line_net_amount'] = BaseFieldDefinition::create('decimal')
      ->setLabel(new TranslatableMarkup('Line net amount'))
      /* @see \Drupal\commerce_price\Plugin\Field\FieldType\PriceItem::schema() */
      ->setSetting('precision', 19)
      ->setSetting('scale', 6)
      ->addPropertyConstraints('value', [
        'BcNotEqualTo' => [
          'number' => '0',
          'scale' => 6,
        ],
      ])
      ->setDisplayOptions('form', [
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'settings' => [
          'scale' => 6,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['invoice_line_vat_category_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t("VAT category code"))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['invoice_line_item_net_price'] = BaseFieldDefinition::create('decimal')
      ->setLabel(new TranslatableMarkup('Line item net price'))
      /* @see \Drupal\commerce_price\Plugin\Field\FieldType\PriceItem::schema() */
      ->setSetting('precision', 19)
      ->setSetting('scale', 6)
      ->setRequired(TRUE)
      ->addPropertyConstraints('value', [
        'BcNotEqualTo' => [
          'number' => '0',
          'scale' => 6,
        ],
      ])
      ->setDisplayOptions('form', [
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'settings' => [
          'scale' => 6,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
