<?php

namespace Drupal\einvoice\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Invoice contact entities.
 *
 * @ingroup einvoice
 */
interface InvoiceContactInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Invoice contact name.
   *
   * @return string
   *   Name of the Invoice contact.
   */
  public function getName();

  /**
   * Sets the Invoice contact name.
   *
   * @param string $name
   *   The Invoice contact name.
   *
   * @return \Drupal\einvoice\Entity\InvoiceContactInterface
   *   The called Invoice contact entity.
   */
  public function setName($name);

  /**
   * Gets the Invoice contact creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Invoice contact.
   */
  public function getCreatedTime();

  /**
   * Sets the Invoice contact creation timestamp.
   *
   * @param int $timestamp
   *   The Invoice contact creation timestamp.
   *
   * @return \Drupal\einvoice\Entity\InvoiceContactInterface
   *   The called Invoice contact entity.
   */
  public function setCreatedTime($timestamp);

}
