<?php

namespace Drupal\einvoice\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Invoice contact entities.
 */
class InvoiceContactViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
