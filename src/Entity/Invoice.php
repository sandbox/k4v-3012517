<?php

namespace Drupal\einvoice\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserInterface;

/**
 * Defines the Invoice entity.
 *
 * @ingroup einvoice
 *
 * @ContentEntityType(
 *   id = "invoice",
 *   label = @Translation("Invoice"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\einvoice\InvoiceListBuilder",
 *     "views_data" = "Drupal\einvoice\Entity\InvoiceViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\einvoice\Form\InvoiceForm",
 *       "add" = "Drupal\einvoice\Form\InvoiceForm",
 *       "edit" = "Drupal\einvoice\Form\InvoiceForm",
 *       "delete" = "Drupal\einvoice\Form\InvoiceDeleteForm",
 *     },
 *     "access" = "Drupal\einvoice\InvoiceAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\einvoice\InvoiceHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "invoice",
 *   admin_permission = "administer invoice entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/invoice/{invoice}",
 *     "add-form" = "/admin/structure/invoice/add",
 *     "edit-form" = "/admin/structure/invoice/{invoice}/edit",
 *     "delete-form" = "/admin/structure/invoice/{invoice}/delete",
 *     "collection" = "/admin/structure/invoice",
 *   },
 *   field_ui_base_route = "invoice.settings"
 * )
 */
class Invoice extends ContentEntityBase implements InvoiceInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Invoice entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 100,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 100,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('A description for this invoice.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['invoice_identifier'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Invoice identifier'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['invoice_issue_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Invoice issue date'))
      ->setSettings([
        'datetime_type' => 'date',
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['document_level_textual_note'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Document level textual note'))
      ->setSettings([
        'max_length' => 255,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // TODO: Invoice currency code.

    $fields['seller'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Seller'))
      ->setSetting('target_type', 'invoice_contact')
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['buyer'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Buyer'))
      ->setSetting('target_type', 'invoice_contact')
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // TODO: Delivery date.

    $fields['payment_due_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Payment due date'))
      ->setSettings([
        'datetime_type' => 'date',
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['account_identifier'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Account identifier'))
      ->setDescription('The bank account of the seller (IBAN)')
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['sellers_financial_institution_branch_identifier'] = BaseFieldDefinition::create('string')
      ->setLabel(t("Name of the seller's bank branch"))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['financial_institution_identifier'] = BaseFieldDefinition::create('string')
      ->setLabel(t("BIC of the seller's bank"))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // TODO: Allowances and charges.

    $fields['vat_total_amount'] = BaseFieldDefinition::create('decimal')
      ->setLabel(new TranslatableMarkup('VAT total amount'))
      /* @see \Drupal\commerce_price\Plugin\Field\FieldType\PriceItem::schema() */
      ->setSetting('precision', 19)
      ->setSetting('scale', 6)
      ->setRequired(TRUE)
      ->addPropertyConstraints('value', [
        'BcNotEqualTo' => [
          'number' => '0',
          'scale' => 6,
        ],
      ])
      ->setDisplayOptions('form', [
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'settings' => [
          'scale' => 6,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['amount_due_for_payment'] = BaseFieldDefinition::create('decimal')
      ->setLabel(new TranslatableMarkup('Amount due for payment'))
      /* @see \Drupal\commerce_price\Plugin\Field\FieldType\PriceItem::schema() */
      ->setSetting('precision', 19)
      ->setSetting('scale', 6)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'settings' => [
          'scale' => 6,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['invoice_line'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Invoice lines'))
      ->setSetting('target_type', 'invoice_line')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;

    // TODO: Entity reference on media entity for the invoice PDF.
  }

}
