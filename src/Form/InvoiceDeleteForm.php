<?php

namespace Drupal\einvoice\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Invoice entities.
 *
 * @ingroup einvoice
 */
class InvoiceDeleteForm extends ContentEntityDeleteForm {


}
