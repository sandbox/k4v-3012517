<?php

namespace Drupal\einvoice\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Invoice contact entities.
 *
 * @ingroup einvoice
 */
class InvoiceContactDeleteForm extends ContentEntityDeleteForm {


}
