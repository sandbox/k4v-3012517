<?php

namespace Drupal\einvoice\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Invoice line entities.
 *
 * @ingroup einvoice
 */
class InvoiceLineDeleteForm extends ContentEntityDeleteForm {


}
