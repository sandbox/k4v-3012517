<?php

namespace Drupal\einvoice;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Invoice line entity.
 *
 * @see \Drupal\einvoice\Entity\InvoiceLine.
 */
class InvoiceLineAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\einvoice\Entity\InvoiceLineInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished invoice line entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published invoice line entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit invoice line entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete invoice line entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add invoice line entities');
  }

}
