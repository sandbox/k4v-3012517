<?php

namespace Drupal\einvoice;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Invoice contact entity.
 *
 * @see \Drupal\einvoice\Entity\InvoiceContact.
 */
class InvoiceContactAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\einvoice\Entity\InvoiceContactInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished invoice contact entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published invoice contact entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit invoice contact entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete invoice contact entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add invoice contact entities');
  }

}
