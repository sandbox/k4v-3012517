<?php

/**
 * @file
 * Contains invoice_contact.page.inc.
 *
 * Page callback for Invoice contact entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Invoice contact templates.
 *
 * Default template: invoice_contact.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_invoice_contact(array &$variables) {
  // Fetch InvoiceContact Entity Object.
  $invoice_contact = $variables['elements']['#invoice_contact'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
